<?php
declare(strict_types=1);

/**
 * This file is part of Harbinger Project.
 *
 * Copyright (c) 2015, Gabriel Heming <gabriel.heming@hotmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Gabriel Heming nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @copyright 2015 Gabriel Heming. All rights reserved.
 * @license http://www.opensource.org/licenses/bsd-license.php BSD License
 **/

namespace Harbinger\Iterator;

/**
 * Abstract collection for objects
 * @package Harbinger
 * @subpackage Iterator
 * @author Gabriel Heming <gabriel.heming@hotmail.com>
 * @see http://php.net/manual/en/class.iterator.php Documentation of Iterator
 * @see http://php.net/manual/en/class.seekableiterator.php Documentation of SeekableIterator
 * @see http://php.net/manual/en/class.countable.php Documentation of Countable
 **/
abstract class Collection implements \SeekableIterator , \Countable
{

    /**
     * Stores the target class of collection
     * @var \Object[]
     **/
    protected $object = [];

    /**
     * Stores the pointer position
     * @var int
     **/
    private $pointer = 0;

    /**
     * Stores the amount of objects
     * @var int
     **/
    protected $total = 0;

    /**
     * Add an Object into collection
     * @param \Object $object
     * @return $this
     * @throws \Harbinger\Iterator\UnexpectedValueException If Object isn't part of an object kind
     * @see \Harbinger\Iterator\Collection::getTargetClass() Retrieve the target class for collection
     **/
    public function add($object) : Collection
    {
        $class = $this->getTargetClass();

        if (!$object instanceof $class) {
            throw new UnexpectedValueException(sprintf("This is a %s collection" , $class));
        }

        $this->object[$this->total] = $object;
        $this->total++;

        return $this;
    }

    /**
     * Get a object from list
     * @param int $index Index of object
     * @return \Object
     * @throws \Harbinger\Iterator\OutOfBoundsException If $index not exists as a object index
     **/
    protected function getRow($index)
    {
        if (!$this->isValid($index)) {
            throw new OutOfBoundsException(sprintf("Index %d not exists as a object index" , $index));
        }

        return $this->object[$index];
    }

    /**
     * Retrieve the object from current position
     * @return \Object
     * @throws \Harbinger\Iterator\OutOfBoundsException If the collection not has any object
     **/
    public function current()
    {
        return $this->getRow($this->key());
    }

    /**
     * Retrieve the current key
     * @return int
     **/
    public function key() : int
    {
        return $this->pointer;
    }

    /**
     * Move the pointer to next index position
     **/
    public function next()
    {
        $this->pointer++;
    }

    /**
     * Move the pointer to beginning
     **/
    public function rewind()
    {
        $this->pointer = 0;
    }

    /**
     * Check if the actual position is valid
     * @return boolean
     **/
    public function valid() : bool
    {
        return $this->isValid($this->key());
    }

    /**
     * Seeks to a given position in the iterator.
     * @param int $position
     * @throws OutOfBoundsException If an invalid position has been given
     **/
    public function seek($position)
    {
        if (!$this->isValid($position)) {
            throw new OutOfBoundsException(sprintf("invalid seek position (%d)" , $position));
        }

        $this->pointer = $position;
    }

    /**
     * Check if given position is valid
     * @param int $position
     * @return boolean
     **/
    protected function isValid(int $position) : bool
    {
        return isset($this->object[$position]);
    }

    /**
     * Retrieve the number of rows
     * @return int
     **/
    public function count() : int
    {
        return (int)$this->total;
    }

    /**
     * Seek and retrieve the last entry from collection
     * @return \Object
     **/
    public function last()
    {
        $this->seek($this->count() - 1);
        return $this->current();
    }

    /**
     * Return the object kind for collection
     * @return string
     **/
    abstract public function getTargetClass() : string;
}
