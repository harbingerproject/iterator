# Iterator

`Harbinger\Iterator` is a library for collection control. It's loosely based on typed lists from strongly typed languages (such Java, C#, etc.).

## Requirements

* PHP 7.0+ (Version `2.0.0` or earlier)
* PHP 5.6+ (Version `1.x.x`)

Installation
---

Package avaible at [Packgist](http://packagist.org)

composer.json
```
#!json
{
    "require": {
        "harbinger/iterator": "@stable",
    }
}
```

Usage
---

As a simple object typed list (eg.: Connection list)
```
#!php
$collection = new \Harbinger\Iterator\Collection\Object(\Connection::class);

$collection->add(new \Connection('pgsql'));
$collection->add(new \Connection('mysql'));

foreach($collection AS $connection) {

}
```
This implementation kind doesn't need any other implementation, but cannot be type hinted for a specified collection list.

Must be validate with `\Harbinger\Iterator\Collection::getTargetClass()`.

```
#!php
$collection->getTargetClass() === \Connection::class;
```

As a modeled object typed list (eg.: Connection list)

```
#!php
namespace Collection;

class Connection extends \Harbinger\Iterator\Collection {

    /**
     * {@inheritdoc}
     **/
    public function getTargetClass() {
        return \Contato::class;
    }
}
```

```
#!php
$collection = new \Collection\Connection();

$collection->add(new \Connection('pgsql'));
$collection->add(new \Connection('mysql'));

foreach($collection AS $connection) {

}
```

Must be implemented for the specified object. Can use type hint:
```
#!php
public function setCollection(\Collection\Connection $collection) {

}
```

## Changelog

### 3.0.0 ###
* Added method `last` to seek and retrieve the last element from collection

### 3.0.0-RC ###
* Removed `\Harbinger\Iterator\Filter\Custom`. It's better use the CallbackFilterIterator from SPL

### 2.0.0 ###
* Implementation for PHP 7 support;
* Add new test for `\Harbinger\Iterator\Filter\Custom`.

#### 1.0.7 ####
* Added method `last` to seek and retrieve the last element from collection

### 1.0.6 ###
* Add new test for `\Harbinger\Iterator\Filter\Custom`.

### 1.0.5 ###
* Updated style coding to match with PSR-1 and PSR-2 (except spaces before comma: interfaces and functions args);
* Removed phpDocumentor version tag.

### 1.0.4 ###
* Construct from `\Harbinger\Iterator\Collection\Object` throw an UnexceptedValueException when a non string or empty string is given as parameter;
* Refactored some phpdoc version blocks.

### 1.0.3 ###
* License information corrected.

### 1.0.2 ###
* License information corrected;
* Corrected `composer.json`.

### 1.0.1 ###
* Corrected `composer.json`;
* Refactored some phpdoc blocks.

### 1.0.0 ###
* Project started.