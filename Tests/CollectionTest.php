<?php
declare(strict_types=1);

namespace Harbinger\Iterator\Test;

use \Harbinger\Iterator\Collection;
use \PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{

    /**
     * @var \Harbinger\Iterator\Collection
     **/
    private $stub;

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Collection::class) , 'Class not found: '.$class);
    }

    public function setUp()
    {
        $this->stub = $this->getMockForAbstractClass(Collection::class);

        $this->stub->expects($this->any())
                   ->method('getTargetClass')
                   ->will($this->returnValue(\stdClass::class));
    }

    public function testIteratorInterfacesMethodsShouldWork()
    {
        $this->assertTrue($this->stub instanceof \Iterator , 'This is not an Iterator object');
        $this->assertTrue(method_exists($this->stub , 'add') , 'There is no valid current on object');
        $this->assertTrue(method_exists($this->stub , 'key') , 'There is no key current on object');
        $this->assertTrue(method_exists($this->stub , 'current') , 'There is no method current on object');
        $this->assertTrue(method_exists($this->stub , 'rewind') , 'There is no rewind current on object');
        $this->assertTrue(method_exists($this->stub , 'valid') , 'There is no valid current on object');

        $this->assertEquals(0 , $this->stub->key() , 'The object isn\'t moved pointer correctly');
        $this->stub->next();
        $this->assertEquals(1 , $this->stub->key() , 'The object isn\'t moved pointer correctly');
        $this->stub->rewind();
        $this->assertEquals(0 , $this->stub->key() , 'The object isn\'t moved pointer correctly');
        $this->assertFalse($this->stub->valid() , 'This is not a SeekableIterator object');

        $object = new \stdClass();
        $this->stub->add($object);

        $this->assertTrue($this->stub->valid() , 'This is not a SeekableIterator object');
        $this->assertTrue($this->stub->current() instanceof \stdClass , 'Not returned the correct object');
        $this->assertSame($this->stub->current() , $object , 'Not returned the correct object');

    }

    public function testCountableInterfaceShouldWork()
    {
        $this->assertTrue($this->stub instanceof \Countable , 'This is not a countable object');
        $this->assertTrue(method_exists($this->stub , 'count') , 'There is no method count on object');
        $this->assertEquals(0 , count($this->stub));
    }

    /**
     * @depends testIteratorInterfacesMethodsShouldWork
     * @depends testCountableInterfaceShouldWork
     **/
    public function testAddAValidObjectShouldWorkAndRetrieveAnInfluentInterface()
    {
        $return = $this->stub->add(new \stdClass());

        $this->assertEquals(1 , count($this->stub) , 'The object isn\'t added to collection');
        $this->assertSame($return , $this->stub , 'Not returned the fluent interface');
    }

    /**
     * @depends testAddAValidObjectShouldWorkAndRetrieveAnInfluentInterface
     **/
    public function testSeekableIteratorInterfaceShouldWork()
    {
        $this->assertTrue($this->stub instanceof \SeekableIterator , 'This is not a SeekableIterator object');
        $this->assertTrue(method_exists($this->stub , 'seek') , 'There is no method seek on object');

        try {
            $this->stub->seek(1);

            $this->fail('An expected exception wasn\'t throwed');
        } catch (\Harbinger\Iterator\OutOfBoundsException $exception) {
        }

        for ($i = 0 ; $i < 10 ; $i++) {
            $list[] = new \stdClass();
        }

        foreach ($list as $row) {
            $this->stub->add($row);
        }

        $this->stub->seek(5);

        $this->assertNotSame($this->stub->current() , $list[4] , 'Returned an invalid object');
        $this->assertSame($this->stub->current() , $list[5] , 'Not returned the same object');
    }

    /**
     * @depends testAddAValidObjectShouldWorkAndRetrieveAnInfluentInterface
     * @expectedException \Harbinger\Iterator\UnexpectedValueException
     **/
    public function testAddAnInvalidObjectShouldThrowAnException()
    {
        $this->stub->add($this);
    }

    /**
     *
     **/
    public function testLastMethod()
    {
        $this->stub->add($object = new \stdClass());
        $last = $this->stub->last();
        $this->assertTrue(is_object($last) , 'Returned an invalid object');
        $this->assertSame($object , $last , 'Not returned the same object');
    }
}
