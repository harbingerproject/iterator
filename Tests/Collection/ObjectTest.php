<?php
declare(strict_types=1);

namespace Harbinger\Iterator\Collection\Test;

use Harbinger\Iterator\Collection;
use \PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{

    /**
     * @var \Harbinger\Iterator\Collection
     **/
    private $objectCollection;

    public function assertPreConditions()
    {
        $this->assertTrue(class_exists($class = Collection\Object::class) , 'Class not found: '.$class);
    }

    public function testConstructObjectCollectionWithRightArgumentsShouldWork()
    {
        $this->objectCollection = new Collection\Object('Collection');
    }

    public function testConstructObjectCollectionWithWrongArgumentsShouldThrowAnException()
    {
        try {
            $this->objectCollection = new Collection\Object(00.01);

            $this->fail('An expected exception wasn\'t throwed');
        } catch (\UnexpectedValueException $exception) {
        } catch (\TypeError $exception) {
        }

        try {
            $this->objectCollection = new Collection\Object('');

            $this->fail('An expected exception wasn\'t throwed');
        } catch (\UnexpectedValueException $exception) {
        }

        try {
            $this->objectCollection = new Collection\Object(null);

            $this->fail('An expected exception wasn\'t throwed');
        } catch (\TypeError $exception) {
        }
    }
}
